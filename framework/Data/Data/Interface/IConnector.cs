﻿// MVC+ framework library for .NET

namespace Framework.Data
{
    #region NamespaceDoc
    /// <summary>
    /// Data namespace contains database abstract.
    /// </summary>
    class NamespaceDoc
    {
    }
    #endregion

    #region interface IConnector
    /// <summary>
    /// database connection interface.
    /// </summary>
    public interface IConnector
    {
        /// <summary>
        /// gets database adapter.
        /// </summary>
        /// <returns>database adapter.</returns>
        IAdapter Adapter { get; }

        /// <summary>
        /// opens database connection.
        /// </summary>
        void Open();

        /// <summary>
        /// closes database connection.
        /// </summary>
        void Close();
    }
    #endregion
}
