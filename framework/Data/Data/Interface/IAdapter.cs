﻿// MVC+ framework library for .NET

using System;

namespace Framework.Data
{
    #region interface IAdapter
    /// <summary>
    /// database adapter interface.
    /// </summary>
    public interface IAdapter
    {
        /// <summary>
        /// gets database identity query statement.
        /// </summary>
        /// <returns>atabase identity query statement.</returns>
        String GetIdentity();

        /// <summary>
        /// gets database single-quote string.
        /// </summary>
        /// <param name="pValue">string value.</param>
        /// <returns>database single-quote string value.</returns>
        String GetString(String pValue);

        /// <summary>
        /// gets database boolean integer value.
        /// </summary>
        /// <param name="pValue">boolean value.</param> 
        /// <returns>database boolean integer value</returns>
        Int16 GetBoolean(Boolean pValue);

        /// <summary>
        /// gets database datetime formatted string.
        /// </summary>
        /// <param name="pDateTime">datetime value.</param>
        /// <returns>database datetime formatted string.</returns>
        String GetDateTime(DateTime pDateTime);

        /// <summary>
        /// gets database IS NULL statement.
        /// </summary>
        /// <param name="pColumn">database column.</param>
        /// <returns>database IS NULL statement.</returns>
        String GetIsNull(String pColumn);

        /// <summary>
        /// gets database IS NOT NULL statement.
        /// </summary>
        /// <param name="pColumn">database column.</param>
        /// <returns>database IS NOT NULL statement.</returns>
        String GetIsNotNull(String pColumn);
    }
    #endregion
}
