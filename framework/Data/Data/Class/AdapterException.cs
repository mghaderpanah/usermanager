﻿// MVC+ framework library for .NET

using Framework.Component;

namespace Framework.Data
{
    #region enum AdapterException
    /// <summary>
    /// exceptions for adapter.
    /// </summary>
    public enum AdapterException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("Adapter::GetString() ... getting database single-quote string.")]
        ERROR_GET_STRING,

        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("Adapter::GetBoolean() ... getting database boolean integer value.")]
        ERROR_GET_BOOLEAN,

        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("Adapter::GetDateTime() ... getting database datetime formatted string.")]
        ERROR_GET_DATETIME,

        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("Adapter::GetIsNull() ... getting database IS NULL statement.")]
        ERROR_GET_ISNULL,

        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("Adapter::GetIsNotNull() ... getting database IS NOT NULL statement.")]
        ERROR_GET_ISNOTNULL,
    }
    #endregion
}
