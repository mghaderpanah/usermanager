﻿// MVC+ framework library for .NET

using System;
using System.Data.Common;

namespace Framework.Data
{
    #region abstract class Adapter
    /// <summary>
    /// database adapter abstract class.
    /// </summary>
    public abstract class Adapter : IAdapter
    {
        /// <summary>
        /// database command.
        /// </summary>
        public DbCommand Command { get; set; }

        /// <summary>
        /// creates database stored-procedute parameter.
        /// </summary>
        /// <returns>database stored-procedute parameter.</returns>
        public DbParameter Parameter { get; set; }

        /// <summary>
        /// gets database identity query statement
        /// </summary>
        /// <returns>database identity query statement</returns>
        public abstract String GetIdentity();

        /// <summary>
        /// gets database single-quote string.
        /// </summary>
        /// <param name="pValue">string value.</param>
        /// <returns>database single-quote string value.</returns>
        public abstract String GetString(String pValue);

        /// <summary>
        /// gets database boolean integer value.
        /// </summary>
        /// <param name="pValue">boolean value.</param> 
        /// <returns>database boolean integer value</returns>
        public abstract Int16 GetBoolean(Boolean pValue);

        /// <summary>
        /// gets database datetime formatted string.
        /// </summary>
        /// <param name="pDateTime">datetime value.</param>
        /// <returns>database datetime formatted string.</returns>
        public abstract String GetDateTime(DateTime pDateTime);

        /// <summary>
        /// gets database IS NULL statement.
        /// </summary>
        /// <param name="pColumn">database column.</param>
        /// <returns>database IS NULL statement.</returns>
        public abstract String GetIsNull(String pColumn);

        /// <summary>
        /// gets database IS NOT NULL statement.
        /// </summary>
        /// <param name="pColumn">database column.</param>
        /// <returns>database IS NULL statement.</returns>
        public abstract String GetIsNotNull(String pColumn);
    }
    #endregion
}
