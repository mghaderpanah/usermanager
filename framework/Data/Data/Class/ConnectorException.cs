﻿// MVC+ framework library for .NET

using Framework.Component;

namespace Framework.Data
{
    #region enum ConnectorException
    /// <summary>
    /// exceptions for connector.
    /// </summary>
    public enum ConnectorException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// get connection string exception.
        /// </summary>
        [EnumStringValue("Connector::GetConnectionString() ... getting connection string.")]
        ERROR_GET_CONNECTION_STRING,

        /// <summary>
        /// open connection exception.
        /// </summary>
        [EnumStringValue("Connector::Open() ... openning connection.")]
        ERROR_OPEN,

        /// <summary>
        /// close connection exception.
        /// </summary>
        [EnumStringValue("Connector::Close() ... closing connection.")]
        ERROR_CLOSE,

        /// <summary>
        /// initialize exception.
        /// </summary>
        [EnumStringValue("Connector.Initialize() ... initializing database connection.")]
        ERROR_INITIALIZE,

        /// <summary>
        /// release exception.
        /// </summary>
        [EnumStringValue("Connector.Release() ... releasing database connection.")]
        ERROR_RELEASE,
    }
    #endregion
}
