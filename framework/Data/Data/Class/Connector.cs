﻿// MVC+ framework library for .NET

using System;
using System.Data.Common;
using Framework.Component;

namespace Framework.Data
{
    #region abstract class Connector
    /// <summary>
    /// database connector abstract class.
    /// </summary>
    public abstract class Connector : IConnector, IInitialize<DbCfg>, IDisposable
    {
        /// <summary>
        /// database connection object.
        /// </summary>
        protected DbConnection Connection { get; set; }

        /// <summary>
        /// indicates this object is already disposed.
        /// </summary>
        private bool isDisposed = false;

        /// <summary>
        /// gets connection string.
        /// </summary>
        /// <param name="pConfig">database configuration.</param>
        /// <returns>connection string.</returns>
        protected abstract String GetConnectionString(DbCfg pConfig);

        /// <summary>
        /// creates database adapter.
        /// </summary>
        /// <returns>database adapter</returns>
        public abstract IAdapter Adapter { get; }

        /// <summary>
        /// opens a database connection.
        /// </summary>
        public void Open()
        {
            try
            {
                if (Connection != null && Connection.State == System.Data.ConnectionState.Closed)
                    Connection.Open();
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<ConnectorException>(EnumHelper.Value(ConnectorException.ERROR_OPEN), ex, true);
            }
        }

        /// <summary>
        /// closes a datanase connection. 
        /// </summary>
        public void Close()
        {
            try
            {
                if (Connection != null && Connection.State == System.Data.ConnectionState.Open)
                    Connection.Close();
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<ConnectorException>(EnumHelper.Value(ConnectorException.ERROR_CLOSE), ex, true);
            }
        }

        /// <summary>
        /// initializes this object respect to database configuration.
        /// </summary>
        /// <param name="pConfig">database configuration.</param>
        public void Initialize(DbCfg pConfig)
        {
            try
            {
                // only support local database connection, for now 
                if (pConfig.PoolType == "local")
                {
                    Connection.ConnectionString = GetConnectionString(pConfig);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<ConnectorException>(EnumHelper.Value(ConnectorException.ERROR_INITIALIZE), ex, true);
            }
        }

        /// <summary>
        /// releases database connection.
        /// </summary>
        public void Release()
        {
            try
            {
                Close();
                Connection = null;
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<ConnectorException>(EnumHelper.Value(ConnectorException.ERROR_RELEASE), ex, true);
            }
        }

        /// <summary>
        /// override dispose.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// override dispose.
        /// </summary>
        /// <param name="pDisposing">disposing option.</param>
        protected virtual void Dispose(bool pDisposing)
        {
            if (isDisposed) return;

            if (pDisposing)
            {
                //managed resources.
                Release();
            }

            // unmanaged resources
            //...;

            isDisposed = true;

            // If it is available, make the call to the  base class's Dispose(Boolean) method
            //base.Dispose(pDisposing);
        }

        /// <summary>
        /// default destructor.
        /// </summary>
        ~Connector()
        {
            Dispose(false);
        }
    }
    #endregion
}
