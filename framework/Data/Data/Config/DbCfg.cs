﻿// MVC+ framework library for .NET

using System;

namespace Framework.Data
{
    #region sealed class: DbCfg
    /// <summary>
    /// database configuration fields enumeration class.
    /// </summary>
    public sealed class DbCfg
    {
        /// <summary>
        /// database type.
        /// </summary>
        public String DBMS;

        /// <summary>
        /// database server name.
        /// </summary>
        public String ServerName;

        /// <summary>
        /// database catalog name.
        /// </summary>
        public String CatalogName;

        /// <summary>
        /// database username.
        /// </summary>
        public String Username;

        /// <summary>
        /// database password.
        /// </summary>
        public String Password;

        /// <summary>
        /// database connection timeout (sec).
        /// </summary>
        public String ConnTimeout;

        /// <summary>
        /// Indicates whether or not using database pool.
        /// </summary>
        public String Pooling;

        /// <summary>
        /// database pool type.
        /// </summary>
        public String PoolType;
    }
    #endregion
}