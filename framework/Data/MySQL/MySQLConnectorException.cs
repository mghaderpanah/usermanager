﻿// MVC+ framework library for .NET

using Framework.Component;

namespace Framework.Data.MySQL
{
    #region enum MySQLConnectorException
    /// <summary>
    /// exceptions for mysql connector.
    /// </summary>
    public enum MySQLConnectorException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("MySQLConnector() ... instantiation of mysql connector.")]
        ERROR_CONSTRUCTOR
    }
    #endregion
}
