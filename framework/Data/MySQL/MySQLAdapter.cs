﻿// MVC+ framework library for .NET

using System;
using System.Text;

namespace Framework.Data.MySQL
{
    #region class MySQLAdapter
    /// <summary>
    /// mysql adapter class.
    /// </summary>
    public class MySQLAdapter : Adapter
    {
        /// <summary>
        /// gets database identity query statement.
        /// </summary>
        /// <returns>database identity query statement</returns>
        public override String GetIdentity()
        {
            return " SELECT LAST_INSERT_ID(); ";
        }

        /// <summary>
        /// gets database single-quote string.
        /// </summary>
        /// <param name="pValue">string value.</param>
        /// <returns>database single-quote string value.</returns>
        public override String GetString(String pValue)
        {
            pValue = pValue.Replace("'", "''");
            StringBuilder value = new StringBuilder(" ");
            value.Append(pValue).Append(" ");
            return value.ToString();
        }

        /// <summary>
        /// gets database boolean integer value.
        /// </summary>
        /// <param name="pValue">boolean value.</param> 
        /// <returns>database boolean integer value</returns>
        public override Int16 GetBoolean(Boolean pValue)
        {
            return (pValue == false) ? Convert.ToInt16(0) : Convert.ToInt16(1);
        }

        /// <summary>
        /// gets database datetime formatted string.
        /// </summary>
        /// <param name="pDateTime">datetime value.</param>
        /// <returns>database datetime formatted string.</returns>
        public override String GetDateTime(DateTime pDateTime)
        {
            StringBuilder value = new StringBuilder(" '");
            value.Append(String.Format("{0:yyyy'-'MM'-'dd' 'HH':'mm':'ss}", pDateTime)).Append("' ");
            return value.ToString();
        }

        /// <summary>
        /// gets database IS NULL statement.
        /// </summary>
        /// <param name="pColumn">database column.</param>
        /// <returns>database IS NULL statement.</returns>
        public override String GetIsNull(String pColumn)
        {
            StringBuilder value = new StringBuilder(" ");
            value.Append(pColumn).Append(" IS NULL ");
            return value.ToString();
        }

        /// <summary>
        /// gets database IS NOT NULL statement.
        /// </summary>
        /// <param name="pColumn">database column.</param>
        /// <returns>database IS NOT NULL statement.</returns>
        public override String GetIsNotNull(String pColumn)
        {
            StringBuilder value = new StringBuilder(" ");
            value.Append(pColumn).Append(" IS NOT NULL ");
            return value.ToString();
        }
    }
    #endregion
}
