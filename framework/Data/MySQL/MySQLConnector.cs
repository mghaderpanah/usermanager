﻿// MVC+ framework library for .NET

using System;
using System.Text;
using MySql.Data.MySqlClient;
using Framework.Component;

namespace Framework.Data.MySQL
{
    #region class MySQLConnector
    /// <summary>
    /// mysql connector class.
    /// </summary>
    public class MySQLConnector : Connector
    {
        #region NamespaceDoc
        /// <summary>
        /// mysql namespace contains mysql database classes.
        /// </summary>
        class NamespaceDoc
        {
        }
        #endregion

        /// <summary>
        /// overload conctructor.
        /// </summary>
        /// <param name="pConfig">database configuration.</param>
        public MySQLConnector(DbCfg pConfig)
        {
            try
            {
                Connection = new MySqlConnection();
                Initialize(pConfig);
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<MySQLConnectorException>(EnumHelper.Value(MySQLConnectorException.ERROR_CONSTRUCTOR), ex, true);
            }
        }

        /// <summary>
        /// gets database adapter.
        /// </summary>
        /// <returns>database adapter.</returns>
        public override IAdapter Adapter
        {
            get
            {
                MySQLAdapter adapter = new MySQLAdapter();
                adapter.Command = this.Connection.CreateCommand();
                adapter.Command.Connection = this.Connection;
                adapter.Parameter = new MySqlParameter();
                return adapter;
            }
        }

        /// <summary>
        /// gets mysql connection string.
        /// </summary>
        /// <param name="pConfig">database configuration.</param>
        /// <returns>mysql connection string.</returns>
        protected override String GetConnectionString(DbCfg pConfig)
        {
            try
            {
                StringBuilder connectionString = new StringBuilder();
                connectionString.Append("server=");
                connectionString.Append(pConfig.ServerName);
                connectionString.Append(";database=");
                connectionString.Append(pConfig.CatalogName);
                connectionString.Append(";uid=");
                connectionString.Append(pConfig.Username);
                connectionString.Append(";password=");
                connectionString.Append(pConfig.Password);
                connectionString.Append(";Connect Timeout=");
                connectionString.Append(pConfig.ConnTimeout);
                connectionString.Append(";Pooling=");
                connectionString.Append(pConfig.Pooling);
                connectionString.Append(";");
                return connectionString.ToString();
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<ConnectorException>(EnumHelper.Value(ConnectorException.ERROR_GET_CONNECTION_STRING), ex, true);
            }
        }
    }
    #endregion
}
