﻿// MVC+ framework library for .NET

using System;
using System.Text;
using System.Data.SqlClient;
using Framework.Component;

namespace Framework.Data.SQLServer
{
    #region class SQLServerConnector
    /// <summary>
    /// sqlserver connector class.
    /// </summary>
    public class SQLServerConnector : Connector
    {
        #region NamespaceDoc
        /// <summary>
        /// sqlserver namespace contains sqlserver database classes.
        /// </summary>
        class NamespaceDoc
        {
        }
        #endregion

        /// <summary>
        /// creates sqlserver connection.
        /// </summary>
        /// <param name="pConfig">database configuration.</param>
        public SQLServerConnector(DbCfg pConfig)
        {
            try
            {
                Connection = new SqlConnection();
                Initialize(pConfig);
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<SQLServerConnectorException>(EnumHelper.Value(SQLServerConnectorException.ERROR_CONSTRUCTOR), ex, true);
            }
        }

        /// <summary>
        /// gets database adapter.
        /// </summary>
        /// <returns>database adapter.</returns>
        public override IAdapter Adapter
        {
            get
            {
                SQLServerAdapter adapter = new SQLServerAdapter();
                adapter.Command = this.Connection.CreateCommand();
                adapter.Command.Connection = this.Connection;
                adapter.Parameter = new SqlParameter();
                return adapter;
            }
        }

        /// <summary>
        /// gets sqlserver connection string.
        /// </summary>
        /// <param name="pConfig">database configuration.</param>
        /// <returns>sqlserver connection string.</returns>
        protected override String GetConnectionString(DbCfg pConfig)
        {
            try
            {
                StringBuilder connectionString = new StringBuilder();
                connectionString.Append("persist security info=False");
                connectionString.Append(";data source=");
                connectionString.Append(pConfig.ServerName);
                connectionString.Append(";initial catalog=");
                connectionString.Append(pConfig.CatalogName);
                connectionString.Append(";user id=");
                connectionString.Append(pConfig.Username);
                connectionString.Append(";password=");
                connectionString.Append(pConfig.Password);
                connectionString.Append(";Connect Timeout=");
                connectionString.Append(pConfig.ConnTimeout);
                connectionString.Append(";Pooling=");
                connectionString.Append(pConfig.Pooling);
                connectionString.Append(";");
                return connectionString.ToString();
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<ConnectorException>(EnumHelper.Value(ConnectorException.ERROR_GET_CONNECTION_STRING), ex, true);
            }
        }
    }
    #endregion
}
