﻿// MVC+ framework library for .NET

using Framework.Component;

namespace Framework.Data.SQLServer
{
    #region enum SQLServerConnectorException
    /// <summary>
    /// exceptions for sqlserver connector.
    /// </summary>
    public enum SQLServerConnectorException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,
        
        /// <summary>
        /// constructor exception.
        /// </summary>
        [EnumStringValue("SQLServerConnector() ... instantiation of sqlserver connector.")]
        ERROR_CONSTRUCTOR
    }
    #endregion
}
