﻿// MVC+ framework library for .NET

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Component;
using Framework.Data.SQLServer;

namespace Framework.Data.SQLServerTest
{
    #region NamespaceDoc
    /// <summary>
    /// sqlservertest namespace contains unittests for sqlserver database classes.
    /// </summary>
    class NamespaceDoc
    {
    }
    #endregion

    /// <summary>
    /// unittest for sqlserver database adapter.
    /// </summary>
    [TestClass]
    public class SQLServerAdapterTest
    {
        /// <summary>
        /// default constructor.
        /// </summary>
        public SQLServerAdapterTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// database configuration.
        /// </summary>
        public static DbConfig dbconfig;

        /// <summary>
        /// gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        /// <summary>
        /// runs code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">test context.</param>
        [ClassInitialize()] 
        public static void MyClassInitialize(TestContext testContext)
        {
            dbconfig = (DbConfig)SerializeHelper.DeSerialize(AppDomain.CurrentDomain.BaseDirectory + "\\config\\DbConfig.config");
            Assert.IsNotNull(dbconfig);
        }

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// test for getting database identity query statement.
        /// </summary>
        [TestMethod]
        public void GetIdentity()
        {
            using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
            {
                String expected = " SELECT SCOPE_IDENTITY(); ";

                IAdapter adapter = conn.Adapter;
                String actual = adapter.GetIdentity();
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        /// test for getting database single-quote string.
        /// </summary>
        [TestMethod]
        public void GetString()
        {
            try
            {
                using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
                {
                    String expected = " ''foo'' ";

                    IAdapter adapter = conn.Adapter;
                    String actual = adapter.GetString("'foo'");
                    Assert.AreEqual(expected, actual);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<AdapterException>(EnumHelper.Value(AdapterException.ERROR_GET_STRING), ex, true);
            }
        }

        /// <summary>
        /// test for getting database boolean integer value.
        /// </summary>
        [TestMethod]
        public void GetBoolean()
        {
            try
            {
                using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
                {
                    Int16 expected = 0;

                    IAdapter adapter = conn.Adapter;
                    Int16 actual = adapter.GetBoolean(false);
                    Assert.AreEqual(expected, actual);

                    expected = 1;
                    actual = adapter.GetBoolean(true);
                    Assert.AreEqual(expected, actual);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<AdapterException>(EnumHelper.Value(AdapterException.ERROR_GET_BOOLEAN), ex, true);
            }
}

/// <summary>
/// test for getting database boolean integer value.
/// </summary>
[TestMethod]
        public void GetDateTime()
        {
            try
            {
                using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
                {
                    String expected = " CONVERT(DATETIME, '2020-11-25 20:27:00' ";

                    IAdapter adapter = conn.Adapter;
                    String actual = adapter.GetDateTime(new DateTime(2020, 11, 25, 20, 27, 00));
                    Assert.AreEqual(expected, actual);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<AdapterException>(EnumHelper.Value(AdapterException.ERROR_GET_DATETIME), ex, true);
            }
        }

/// <summary>
/// test for getting database IS NULL statement.
/// </summary>
[TestMethod]
        public void GetIsNull()
        {
            try
            {
                using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
                {
                    String expected = " mycolumn IS NULL ";

                    IAdapter adapter = conn.Adapter;
                    String actual = adapter.GetIsNull("mycolumn");
                    Assert.AreEqual(expected, actual);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<AdapterException>(EnumHelper.Value(AdapterException.ERROR_GET_ISNULL), ex, true);
            }
        }

        /// <summary>
        /// test for getting database IS NOT NULL statement.
        /// </summary>
        [TestMethod]
        public void GetIsNotNull()
        {
            try
            {
                using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
                {
                    String expected = " mycolumn IS NOT NULL ";

                    IAdapter adapter = conn.Adapter;
                    String actual = adapter.GetIsNotNull("mycolumn");
                    Assert.AreEqual(expected, actual);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<AdapterException>(EnumHelper.Value(AdapterException.ERROR_GET_ISNOTNULL), ex, true);
            }
        }
    }
}
