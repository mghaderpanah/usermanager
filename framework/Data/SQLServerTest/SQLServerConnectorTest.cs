﻿// MVC+ framework library for .NET

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Component;
using Framework.Data.SQLServer;

namespace Framework.Data.SQLServerTest
{
    #region SQLServerConnectorTest
    /// <summary>
    /// unittest for sqlserver database connector.
    /// </summary>
    [TestClass]
    public class SQLServerConnectorTest
    {
        /// <summary>
        /// default constructor.
        /// </summary>
        public SQLServerConnectorTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// database configuration.
        /// </summary>
        public static DbConfig dbconfig;

        /// <summary>
        /// gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        /// <summary>
        /// runs code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">test context.</param>
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                dbconfig = (DbConfig)SerializeHelper.DeSerialize(AppDomain.CurrentDomain.BaseDirectory + "\\config\\DbConfig.config");
                Assert.IsNotNull(dbconfig);
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<SerializeHelperException>(EnumHelper.Value(SerializeHelperException.ERROR_Deserialize), ex, true);
            }
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// test for overload constructor. 
        /// </summary>
        [TestMethod]
        public void OverloadConstructor()
        {
            using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
            { 
                try
                {
                    Assert.IsNotNull(conn);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Assert.Fail();
                }
            }
        }

        /// <summary>
        /// test for openning sqlserver database connection.
        /// </summary>
        [TestMethod]
        public void Open()
        {
            using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Assert.Fail();
                }
            }
        }

        /// <summary>
        /// test for closing sqlserver database connection.
        /// </summary>
        [TestMethod]
        public void Close()
        {
            using (Connector conn = new SQLServerConnector(dbconfig.UserManager))
            {

                try
                {
                    conn.Open();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Assert.Fail();
                }
            }
        }
    }
    #endregion

    /// <summary>
    /// Database configuration enumeration class.
    /// </summary>
    public sealed class DbConfig
    {
        /// <summary>
        /// usermanagement database configuration.
        /// </summary>
        public DbCfg UserManager { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DbConfig()
        {
            UserManager = new DbCfg();
        }
    }
}

