﻿// MVC+ framework library for .NET

using System;
using System.Reflection;

namespace Framework.Component
{
    #region static class EnumHelper
    /// <summary>
    /// enumeration helper class.
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// get string value of the enumeration.
        /// </summary>
        /// <param name="pEnum">enumeration</param>
        /// <returns>string value of the enum.</returns>
        public static String Value(Enum pEnum)
        {
            try
            {
                String output = null;
                Type type = pEnum.GetType();
                FieldInfo fieldinfo = type.GetField(pEnum.ToString());
                EnumStringValue[] attrs = fieldinfo.GetCustomAttributes(typeof(EnumStringValue), false) as EnumStringValue[];

                if (attrs.Length > 0)
                    output = attrs[0].Value;

                return output;
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<EnumHelperException>(EnumHelper.Value(EnumHelperException.ERROR_VALUE), ex, true);
            }
        }
    }
    #endregion
}
