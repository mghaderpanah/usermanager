﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    /// <summary>
    /// log helper exception.
    /// </summary>
    public enum LogHelperException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// falid to tag begin of fucntion call.
        /// </summary>
        [EnumStringValue("LogHelper.DebugBegin() ... log beginning of a function call.")]
        ERROR_BEGIN,

        /// <summary>
        /// failed to tag end of a cuntion call.
        /// </summary>
        [EnumStringValue("LogHelper.DebugEnd() ... log end of a function call.")]
        ERROR_END,

        /// <summary>
        /// failed to tag an information.
        /// </summary>
        [EnumStringValue("LogHelper.Info() ... log binformation.")]
        ERROR_INFO,

        /// <summary>
        /// failed to tag an error.
        /// </summary>
        [EnumStringValue("LogHelper.Error() ... log error.")]
        ERROR_ERR
    }
}
