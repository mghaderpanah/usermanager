﻿// MVC+ framework library for .NET

using System;
using System.Text;

namespace Framework.Component
{
    #region static class StringHelper
    /// <summary>
    /// string helper class.
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// gets substrings of a string respect to defaul delimiter.
        /// <see cref="Constant.DEFAULT_DELIMITER"/>
        /// </summary>
        /// <param name="pString">source string.</param>
        /// <returns>sub strings.</returns>
        public static String[] GetSubStrings(String pString)
        {
            try
            {
                String[] delimiter = new String[] { Constant.DEFAULT_DELIMITER };
                return pString.Split(delimiter, StringSplitOptions.None);
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<StringHelperException>(EnumHelper.Value(StringHelperException.ERROR_GETSUBSTRING), ex, true);
            }
        }

        /// <summary>
        /// appends defaul delimiter into a string.
        /// <see cref="Constant.DEFAULT_DELIMITER"/>
        /// </summary>
        /// <param name="pString">source string.</param>
        /// <returns>delimiter appended string.</returns>
        public static String AppendDelimiter(String pString)
        {
            try
            {
                if (pString.Length <= 0) return pString;

                StringBuilder str = new StringBuilder(pString);
                str.Append(Constant.DEFAULT_DELIMITER);
                return str.ToString();
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<StringHelperException>(EnumHelper.Value(StringHelperException.ERROR_APPEND), ex, true);
            }
        }

        /// <summary>
        /// remvoves default delimiter from the end of a string.
        /// </summary>
        /// <param name="pString">source string.</param>
        /// <returns>delimiter removed string.</returns>
        public static String RemoveDelimeter(String pString)
        {
            try
            {
                if (pString.Length <= 0) return pString;

                StringBuilder str = new StringBuilder(pString);
                int pos = str.Length - Constant.DEFAULT_DELIMITER.Length;
                str.Remove(pos, Constant.DEFAULT_DELIMITER.Length);
                return str.ToString();
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<StringHelperException>(EnumHelper.Value(StringHelperException.ERROR_REMOVE), ex, true);
            }
        }
    }
    #endregion
}
