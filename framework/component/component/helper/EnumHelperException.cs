﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    #region enum EnumHelperException
    /// <summary>
    /// exceptions for EnumHelper.
    /// </summary>
    public enum EnumHelperException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// value exception.
        /// </summary>
        [EnumStringValue("EnumHelper::Value() ... returning string value of enumeration.")]
        ERROR_VALUE
    }
    #endregion
}
