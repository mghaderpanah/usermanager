﻿// MVC+ framework library for .NET

using System;
using System.Text;
using log4net;

namespace Framework.Component
{
    #region static class LogHelper<T>
    /// <summary>
    /// log helper class.
    /// log4net.Config.BasicConfigurator.Configure() must be called in caller application. 
    /// </summary>
    /// <typeparam name="T">logged object type.</typeparam>
    public static class LogHelper<T>
    {
        /// <summary>
        /// log manager object.
        /// </summary>
        static readonly log4net.ILog _log = LogManager.GetLogger(typeof(T).IsGenericType ? typeof(T).GetGenericTypeDefinition() : typeof(T));

        /// <summary>
        /// logs xml header.
        /// </summary>
        public static void Xml()
        {
            try
            {
                if (_log.IsDebugEnabled)
                {
                    String info = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

                    System.Diagnostics.Debug.WriteLine(info);
                    _log.Info(info);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<LogHelperException>(EnumHelper.Value(LogHelperException.ERROR_END), ex, true);
            }
        }

        /// <summary>
        /// logs beginning of a function call.
        /// </summary>
        /// <param name="pInfo">function information.</param>
        /// <param name="pIncludeDateTime">include datetime tag.</param>/// 
        public static void Debug(String pInfo, Boolean pIncludeDateTime)
        {
            try
            {
                if (_log.IsDebugEnabled)
                {
                    StringBuilder info = new StringBuilder();

                    info.Append("<");
                    info.Append(typeof(T).ToString());
                    info.Append(".");

                    pInfo.Replace(".ctor", ".Constructor");
                    pInfo.Replace("`1", "_");
                    pInfo.Replace("[", "");
                    pInfo.Replace("]", "");
                    info.Append(pInfo);

                    if (pIncludeDateTime)
                        info.Append(" DateTime=\"" + DateTime.Now.ToString("s") + "\"");

                    info.Append(">");

                    System.Diagnostics.Debug.WriteLine(info.ToString());
                    _log.Debug(info.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<LogHelperException>(EnumHelper.Value(LogHelperException.ERROR_BEGIN), ex, true);
            }
        }

        /// <summary>
        /// logs end of a function call.
        /// </summary>
        /// <param name="pInfo">function information.</param>
        public static void Debug(String pInfo)
        {
            try
            {
                if (_log.IsDebugEnabled)
                {
                    StringBuilder info = new StringBuilder();

                    info.Append("</");
                    info.Append(typeof(T).ToString());
                    info.Append(".");

                    pInfo.Replace(".ctor", ".Constructor");
                    pInfo.Replace("`1", "_");
                    pInfo.Replace("[", "");
                    pInfo.Replace("]", "");
                    info.Append(pInfo);

                    info.Append(">");

                    System.Diagnostics.Debug.WriteLine(info.ToString());
                    _log.Debug(info.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<LogHelperException>(EnumHelper.Value(LogHelperException.ERROR_END), ex, true);
            }
        }

        /// <summary>
        /// logs an information.
        /// </summary>
        /// <param name="pInfo">information message.</param>
        public static void Info(String pInfo)
        {
            try
            {
                if (_log.IsDebugEnabled)
                {
                    System.Diagnostics.Debug.WriteLine(pInfo);

                    StringBuilder info = new StringBuilder();
                    info.Append("<");
                    info.Append(pInfo);
                    info.Append("/>");

                    _log.Info(info.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<LogHelperException>(EnumHelper.Value(LogHelperException.ERROR_INFO), ex, true);
            }
        }

        /// <summary>
        /// logs an error.
        /// </summary>
        /// <param name="pError">erro message.</param>
        public static void Error(String pError)
        {
            try
            {
                if (_log.IsDebugEnabled)
                {
                    System.Diagnostics.Debug.WriteLine(pError);

                    StringBuilder error = new StringBuilder();
                    error.Append("<");
                    error.Append(pError);
                    error.Append("/>");

                    _log.Error(error.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<LogHelperException>(EnumHelper.Value(LogHelperException.ERROR_ERR), ex, true);
            }
        }
    }
    #endregion
}
