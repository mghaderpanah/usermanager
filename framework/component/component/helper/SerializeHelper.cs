﻿// MVC+ framework library for .NET

using System;
using System.Yaml.Serialization;

namespace Framework.Component
{
    #region static class SerializeHelper
    /// <summary>
    /// serialize helper class.
    /// </summary>
    public static class SerializeHelper
    {
        /// <summary>
        /// deserializes yaml file into an Object.
        /// </summary>
        /// <param name="pFileName">yaml filepath and filename.</param>
        /// <returns>deserialized Object.</returns>
        public static Object DeSerialize(String pFileName)
        {
            try
            {
                var serializer = new YamlSerializer();
                Object[] resource = serializer.DeserializeFromFile(pFileName);
                return resource[0];
            }
            catch (Exception ex)
            {
                throw new ExceptionEx<SerializeHelperException>(EnumHelper.Value(SerializeHelperException.ERROR_Deserialize), ex, true);
            }
        }
    }
    #endregion
}
