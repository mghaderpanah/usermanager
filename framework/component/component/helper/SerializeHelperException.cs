﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    #region enum: SerializeHelperException
    /// <summary>
    /// serialize helper exception.
    /// </summary>
    public enum SerializeHelperException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// failed deserializing yaml file.
        /// </summary>
        [EnumStringValue("SerializeHelper.Deserialize() ... deserializing.")]
        ERROR_Deserialize
    }
    #endregion
}
