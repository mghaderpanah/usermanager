﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    #region enum StringHelperException
    /// <summary>
    /// string helper exception.
    /// </summary>
    public enum StringHelperException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// failed getting substring.
        /// </summary>
        [EnumStringValue("invalid")]
        ERROR_GETSUBSTRING,

        /// <summary>
        /// failed appending default dlimiter.
        /// </summary>
        [EnumStringValue("invalid")]
        ERROR_APPEND,

        /// <summary>
        /// failed removing default dimilter from end.
        /// </summary>
        [EnumStringValue("invalid")]
        ERROR_REMOVE
    }
    #endregion
}
