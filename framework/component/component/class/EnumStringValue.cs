﻿// MVC+ framework library for .NET

using System;

namespace Framework.Component
{
    #region sealed class EnumStringValue
    /// <summary>
    /// enumeration string class.
    /// </summary>
    public sealed class EnumStringValue : System.Attribute
    {
        #region private members
        /// <summary>
        /// string value.
        /// </summary>
        private String _value;
        #endregion

        #region public members
        /// <summary>
        /// string value internal getter.
        /// </summary>
        internal String Value { get { return _value; } }

        /// <summary>
        /// overloaded constructor.
        /// </summary>
        /// <param name="pValue">string value.</param>
        public EnumStringValue(String pValue)
        {
            _value = pValue;
        }
        #endregion
    }
    #endregion
}
