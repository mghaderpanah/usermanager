﻿// MVC+ framework library for .NET

using System;

namespace Framework.Component
{
    #region abstract class Factory<P,C>
    /// <summary>
    /// factory pattern abstract class.
    /// </summary>
    /// <typeparam name="P">product type</typeparam>
    /// <typeparam name="C">configuration type</typeparam>
    public abstract class Factory<P,C> : Framework.Component.IFactory<P,C>
    {
        /// <summary>
        /// factory name.
        /// </summary>
        private String _name;

        /// <summary>
        /// factory name getter/setter.
        /// </summary>
        protected String Name { get { return _name; } set { _name = value; } }

        /// <summary>
        /// creates instance of the product {P}.
        /// </summary>
        /// <param name="pConfig">configuration object.</param>
        /// <returns>instance of a product.</returns>
        public abstract P CreateProduct(C pConfig);
    }
    #endregion
}
