﻿// MVC+ framework library for .NET

using System;

namespace Framework.Component
{
    #region NamespaceDoc
    /// <summary>
    /// component namespace contains fundamental interfaces and classes that define commonly-used constants, enumerations, exceptions, and helpers.
    /// </summary>
    class NamespaceDoc
    {
    }
    #endregion

    #region static class Constant
    /// <summary>
    /// generic constants class.
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// default invalid date.
        /// </summary>
        public static DateTime DEFAULT_DATE_INVALID = new DateTime(1799, 1, 1);

        /// <summary>
        /// default date format.
        /// </summary>
        public static String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";

        /// <summary>
        /// default datetime format.
        /// </summary>
        public static String DEFAULT_DATETIME_FORMAT = "MM/dd/yyyy hh:mm:ss tt";

        /// <summary>
        /// default string delimiter.
        /// </summary>
        public static String DEFAULT_DELIMITER = ";;";

        /// <summary>
        /// default empty string value.
        /// </summary>
        public static String DEFAULT_STRING_EMPTY = "";

        /// <summary>
        /// default value for false string.
        /// </summary>
        public static String DEFAULT_STRING_FALSE = "-";

        /// <summary>
        /// default value for true string.
        /// </summary>
        public static String DEFAULT_STRING_TRUE = "√";
        
        /// <summary>
        /// default integer value for false.
        /// </summary>
        public static Int16 DEFAULT_FALSE = 0;

        /// <summary>
        /// defualt integer value for true.
        /// </summary>
        public static Int16 DEFAULT_TRUE = 1;

        /// <summary>
        /// default invalid value for Int16.
        /// </summary>
        public static Int16 DEFAULT_INT16_INVALID = Int16.MinValue;

        /// <summary>
        /// default invalid value for Int32.
        /// </summary>
        public static Int32 DEFAULT_INT32_INVALID = Int32.MinValue;

        /// <summary>
        /// default invalid value for Double.
        /// </summary>
        public static Double DEFAULT_DOUBLE_INVALID = Double.MinValue;
    }
    #endregion
}
