﻿using System;
using System.Text;
using System.Runtime.Serialization;

namespace Framework.Component
{
    #region sealed class: ExceptionEx<T>
    /// <summary>
    /// exception class.
    /// </summary>
    /// <typeparam name="T">exception enumeration type.</typeparam>
    public sealed class ExceptionEx<T> : System.Exception
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public ExceptionEx()
            : base()
        {
        }

        /// <summary>
        /// oveloaded constructor.
        /// </summary>
        /// <param name="pMsg">exception message.</param>
        public ExceptionEx(String pMsg)
            : base(pMsg)
        {
        }

        /// <summary>
        /// overloaded constructor respecting inner exception.
        /// </summary>
        /// <param name="pMsg">exception message.</param>
        /// <param name="pInner">inner exception.</param>
        /// <param name="pShowInner">indicate wether or not to show inner exception message.</param>
        public ExceptionEx(String pMsg, System.Exception pInner, Boolean pShowInner = false)
            : base(pMsg, pInner)
        {
            StringBuilder lMsg = new StringBuilder();
            lMsg.AppendLine();
            lMsg.Append(pMsg);

            if (pShowInner)
            {
                lMsg.AppendLine();
                lMsg.Append(pInner.Message);
            }
        }
        /// <summary>
        /// oveloaded constructor.
        /// </summary>
        /// <param name="pInfo">Holds the serialized object data about the exception being thrown.</param>
        /// <param name="pContext">Source and destination of a given serialized stream</param>
        public ExceptionEx(SerializationInfo pInfo, StreamingContext pContext)
            : base(pInfo, pContext)
        {
        }
        #endregion
    }
}
