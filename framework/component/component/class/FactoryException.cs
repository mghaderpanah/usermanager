﻿// MVC+ framework library for .NET

using Framework.Component;

namespace Framework.Component
{
    #region enum FactoryException
    /// <summary>
    /// factory exception.
    /// </summary>
    public enum FactoryException
    {
        /// <summary>
        /// invalid exception.
        /// </summary>
        [EnumStringValue("invalid")]
        INVALID,

        /// <summary>
        /// failed creating product.
        /// <see cref="Factory{T, C}.CreateProduct"/>
        /// </summary>
        [EnumStringValue("Factory::CreateProduct() ... creating instance of the product.")]
        ERROR_CREATE_PRODUCT
    }
    #endregion
}
