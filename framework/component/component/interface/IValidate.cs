﻿// MVC+ framework library for .NET

using System;

namespace Framework.Component
{
    #region interface IValidate<T>
    /// <summary>
    /// validation interface.
    /// </summary>
    /// <typeparam name="T">type of object</typeparam>
    public interface IValidate<T>
    {
        /// <summary>
        /// indicates this object is empty.
        /// </summary>
        /// <returns>true if this object is empty.</returns>
        Boolean IsEmpty();

        /// <summary>
        /// indicates this object is equal to referenced object.
        /// </summary>
        /// <param name="pObject">object to be compared with.</param>
        /// <returns>true if this object is equal to pObject.</returns>
        Boolean IsEqual(T pObject);

        /// <summary>
        /// indicates this object is valid.
        /// </summary>
        /// <returns>true if this object is valid.</returns>
        Boolean IsValid();

        /// <summary>
        /// indicates staring value matches to the validation expression.
        /// </summary>
        /// <param name="pValue">value.</param>
        /// <param name="Validation">validation expression.</param>
        /// <returns>true if value matches to validation expression.</returns>
        Boolean IsMatch(String pValue, String Validation);
    }
    #endregion
}
