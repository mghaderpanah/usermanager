﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    #region interface IClone<T>
    /// <summary>
    /// prototype pattern interface.
    /// </summary>
    /// <typeparam name="T">object type</typeparam>
    public interface IClone<T>
    {
        /// <summary>
        /// clones this object.
        /// </summary>
        /// <returns>clone of this object.</returns>
        T Clone();

        /// <summary>
        /// copies this object.
        /// </summary>
        /// <param name="pObject">copied object.</param>
        void Copy(T pObject);
    }
    #endregion
}
