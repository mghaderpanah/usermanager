﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    #region interface IEnum<K,V>
    /// <summary>
    /// enumeration interface.
    /// </summary>
    /// <typeparam name="K">key.</typeparam>
    /// <typeparam name="V">value</typeparam>
    public interface IEnum<K,V>
    {
        /// <summary>
        /// gets value of the key.
        /// </summary>
        /// <param name="pKey">key.</param>
        /// <returns>value of the key</returns>
        V GetValue(K pKey);

        /// <summary>
        /// gets key of the value.
        /// </summary>
        /// <param name="pValue">value.</param>
        /// <returns>key of the valsue.</returns>
        K GetKey(V pValue);
    }
    #endregion
}
