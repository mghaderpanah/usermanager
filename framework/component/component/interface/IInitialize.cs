﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    #region interface IInitialize<T>
    /// <summary>
    /// un-managed data interface. 
    /// </summary>
    /// <typeparam name="T">configuration type</typeparam>
    public interface IInitialize<T>
    {
        /// <summary>
        /// initializes unmanaged data.
        /// </summary>
        /// <param name="pConfig">configration object.</param>
        void Initialize(T pConfig);

        /// <summary>
        /// releases un-managed data.
        /// </summary>
        void Release();
    }
    #endregion
}
