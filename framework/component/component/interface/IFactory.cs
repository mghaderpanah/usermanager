﻿// MVC+ framework library for .NET

namespace Framework.Component
{
    # region interface IFactory<P,C>
    /// <summary>
    /// factory pattern interface.
    /// </summary>
    /// <typeparam name="P">product type</typeparam>
    /// <typeparam name="C">configuration type</typeparam>
    public interface IFactory<P,C>
    {
        /// <summary>
        /// creates instance of the product {P}.
        /// </summary>
        /// <param name="pConfig">configuration object.</param>
        /// <returns>instance of the product</returns>
        P CreateProduct(C pConfig);
    }
    #endregion
}
