﻿// MVC+ framework library for .NET

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Component;

namespace Framework.ComponentTest
{
    #region class SerializeHelperTest
    /// <summary>
    /// unittest for serialize helper.
    /// </summary>
    [TestClass]
    public class SerializeHelperTest
    {
        /// <summary>
        /// context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// default constructor.
        /// </summary>
        public SerializeHelperTest()
        {
        }

        /// <summary>
        /// gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        
        /// <summary>
        /// unittest for deserialize.
        /// </summary>
        [TestMethod]
        public void DeSerialize()
        {
            Entity entity1 = new Entity
            {
                Attribute1 = "value1_1",
                Attribute2 = "value1_2",
                Attribute3 = "value1_3"
            };

            Entity entity2 = new Entity
            {
                Attribute1 = "value2_1",
                Attribute2 = "value2_2",
                Attribute3 = "value2_3"
            };

            Resource expected = new Resource
            {
                Entity1 = entity1,
                Entity2 = entity2
            };

            String filename = System.AppDomain.CurrentDomain.BaseDirectory + "\\config" + "\\" + "resource.yaml";
            Resource actual = (Resource)SerializeHelper.DeSerialize(filename);

            Assert.AreEqual(expected.Entity1.Attribute1, actual.Entity1.Attribute1);
            Assert.AreEqual(expected.Entity1.Attribute2, actual.Entity1.Attribute2);
            Assert.AreEqual(expected.Entity1.Attribute3, actual.Entity1.Attribute3);
        }
    }
    #endregion

    #region class Entity
    /// <summary>
    /// entity yaml class.
    /// </summary>
    public sealed class Entity
    {
        /// <summary>
        /// first attribute.
        /// </summary>
        public String Attribute1;

        /// <summary>
        /// second attribute.
        /// </summary>
        public String Attribute2;

        /// <summary>
        /// third attribute.
        /// </summary>
        public String Attribute3;
    }
    #endregion

    #region class Resource
    /// <summary>
    /// resource yaml class.
    /// </summary>
    public sealed class Resource
    {
        /// <summary>
        /// first entity.
        /// </summary>
        private Entity _entity1;

        /// <summary>
        /// second entity.
        /// </summary>
        private Entity _entity2;

        /// <summary>
        /// first entity getter.
        /// </summary>
        public Entity Entity1 { get { return _entity1; } set { _entity1 = value; } }

        /// <summary>
        /// second entity getter.
        /// </summary>
        public Entity Entity2 { get { return _entity2; } set { _entity2 = value; } }

        /// <summary>
        /// default constructor.
        /// </summary>
        public Resource()
        {
            _entity1 = new Entity();
            _entity2 = new Entity();
        }
    }
    #endregion
}
