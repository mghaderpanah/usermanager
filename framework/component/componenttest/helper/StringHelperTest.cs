﻿// MVC+ framework library for .NET

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Component;

namespace Framework.ComponentTest
{
    #region class StringHelperTest
    /// <summary>
    /// unittest for string helper class.
    /// </summary>
    [TestClass]
    public class StringHelperTest
    {
        /// <summary>
        /// context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// default constructor.
        /// </summary>
        public StringHelperTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// unittest for get substrings.
        /// </summary>
        [TestMethod]
        public void GetSubStrings()
        {
            String[] expected = { "abc", "def", "ghi" };

            String[] actual = StringHelper.GetSubStrings("abc;;def;;ghi;;");

            for(int i=0; i < actual.Length;  i++)
            {
                if (actual[i].Length > 0)
                {
                    Assert.AreEqual(expected[i], actual[i]);
                }
            }
        }

        /// <summary>
        /// unittest for append delimiter.
        /// </summary>
        [TestMethod]
        public void AppendDelimiter()
        {
            String expected = "abc;;";

            String actual = StringHelper.AppendDelimiter("abc");
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// unittest for remove delimiter.
        /// </summary>
        [TestMethod]
        public void RemoveDelimeter()
        {
            String expected = "abc";

            String actual = StringHelper.RemoveDelimeter("abc;;");
            Assert.AreEqual(expected, actual);

            expected = "abc;;12;";

            actual = StringHelper.RemoveDelimeter("abc;;12;;;");
            Assert.AreEqual(expected, actual);
        }
    }
    #endregion
}
