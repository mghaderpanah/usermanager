﻿// MVC+ framework library for .NET

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Component;

namespace Framework.ComponentTest
{
    #region NamespaceDoc
    /// <summary>
    /// componenttest namespace contains unit tests for Framework.Component namespace.
    /// </summary>
    class NamespaceDoc
    {
    }
    #endregion

    #region class EnumHelperTest
    /// <summary>
    ///unittest for enum helper class.
    /// </summary>
    [TestClass]
    public class EnumHelperTest
    {
        /// <summary>
        /// default constructor.
        /// </summary>
        public EnumHelperTest()
        {
        }

        /// <summary>
        /// context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        
        /// <summary>
        /// unittest for value.
        /// </summary>
        [TestMethod]
        public void Value()
        {
            String expected = "EnumHelper::Value() ... returning string value of enumeration.";

            String actual = EnumHelper.Value(EnumHelperException.ERROR_VALUE);
            Assert.AreEqual(expected, actual);
        }
    }
    #endregion
}
