﻿// MVC+ framework library for .NET

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Component;

namespace Framework.ComponentTest
{
    #region class LogHelperTest
    /// <summary>
    /// unittest for log helper.
    /// </summary>
    [TestClass]
    public class LogHelperTest
    {
        /// <summary>
        /// default constructor.
        /// </summary>
        public LogHelperTest()
        {
        }

        /// <summary>
        /// context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        /// <summary>
        /// runs code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">test context.</param>
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            log4net.Config.BasicConfigurator.Configure();
            LogHelper<LogHelperTest>.Xml();
            LogHelper<LogHelperTest>.Debug("Framework.ComponentTest.LogHelperTest", true);
        }

        /// <summary>
        /// runs code after all tests in a class have run.
        /// </summary>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            LogHelper<LogHelperTest>.Debug("Framework.ComponentTest.LogHelperTest");
        }
        
        // Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        
        /// <summary>
        /// unittest for debug begin.
        /// </summary>
        [TestMethod]
        public void Debug()
        {
            LogHelper<LogHelperTest>.Debug(System.Reflection.MethodBase.GetCurrentMethod().Name, true);
            Info();
            LogHelper<LogHelperTest>.Debug(System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        /// <summary>
        /// unittest for info.
        /// </summary>
        [TestMethod]
        public void Info()
        {
            LogHelper<LogHelperTest>.Info(System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        /// <summary>
        /// unittest for error.
        /// </summary>
        [TestMethod]
        public void Error()
        {
            LogHelper<LogHelperTest>.Error(System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    #endregion
}
